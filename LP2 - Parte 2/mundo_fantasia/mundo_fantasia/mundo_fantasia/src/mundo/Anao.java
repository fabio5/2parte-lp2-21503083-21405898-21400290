package mundo;

public class Anao extends Personagem {

	public Anao(int id, String nome, int orientacao) {
		super(id, nome, orientacao);
		super.tipo = 0;
		super.movimento = 1; 
		super.moveOrientacao = 2;
	}

	@Override
	protected boolean Movimentar(char[] caminho) {
		
		
		// Validamos a primeira e única posicao do caminho
		return posicionar(caminho[0]);
	}
	
	@Override
	protected Boolean fazer_efeito() {
		return false;
	}

}
