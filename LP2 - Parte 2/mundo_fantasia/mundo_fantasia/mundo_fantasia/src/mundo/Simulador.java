package mundo;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;



public class Simulador {

	static private ListaPersonagens personagens;
	static private Mapa mapa;
	
	public static void main(String[] args) throws IOException {

		
		
		
		String selector_ficheiro = "";
		
		// Numero de turnos que vamos simular
		int numTurnos = 5;
		
		// Variavel utilizada para construir os diferentes nomes de ficheiro 
		String nomeFicheiro=null;
		
		try {
			//Ler ficheiro de personagens
			nomeFicheiro = "personagens"+selector_ficheiro+".txt";
			personagens = new ListaPersonagens(nomeFicheiro);
		
			//Ler ficheiro do mapa
			nomeFicheiro = "mapa"+selector_ficheiro+".txt";
			mapa = new Mapa(nomeFicheiro, personagens);
			
		} catch (FileNotFoundException e) {
			System.out.printf("Erro: Não foi encontrado o ficheiro '%s'.\n", nomeFicheiro);
			System.exit(1);
		}
		
		
		
		//Iniciar a simulacao
		correr_simulacao(numTurnos, "resultados"+selector_ficheiro+".txt");
	}
	
	
	private static void correr_simulacao(int num_turnos, String nomeFicheiro) throws IOException {

		FileWriter ficheiro = new FileWriter(nomeFicheiro);
		
		//estado inicial
		ficheiro.write("Estado inicial\n");
		ficheiro.write(mapa.toString()+"\n");
		
		int num_personagens = personagens.getContar();
		
		
		for (int turno=0; turno<num_turnos; turno++) {
			
		
			for (int id=0; id<num_personagens; id++) {
				
				Personagem p = personagens.getPersonagem(id);
				Boolean resultado;
				
				
				resultado = p.mover();
				
				
				if ( resultado==true )
					p.fazer_efeito();
				else
					p.virar();
			}
			
			
			ficheiro.write("Turno " + (turno+1) + "\n");
			ficheiro.write(mapa.toString()+"\n");
		}
		
		
		
		
		ficheiro.write("Tesouros\n");
		ficheiro.write(personagens.toString());
		
		ficheiro.close();
	}

}
